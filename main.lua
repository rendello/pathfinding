#!/usr/bin/lua5.3

local math = require('math')
local inspect = require('inspect')

function get_manhattan_dist(a, b)
	--[[
	Args:
		a: <table> containing x and y coords of object a.
		b: <table> containing x and y coords of object b.
	Returns:
		<int> containing the Manhattan distance between a and b.
	]]
	return (math.abs(a.x, - b.x) + math.abs(a.y - b.y))
end

function coords_to_string(x, y)
	return tostring(x)..':'..tostring(y)
end

function make_graph(map)
	--[[ Gets a list of nodes (passable tiles) and returns their connections.
	Args:
		map: a <table>. Impassable tiles are represtented as math.huge.
	Returns:
		nodes: a <table> containing the <bool> 'visited', set to false be
			default and for use by the algorithms, and a <table> 'neighbors',
			with their stringified neighbor coords as keys and their weights as
			values.
	]]
	nodes = {}
	for x, column in pairs(map) do
		for y, weight in pairs(column) do

			-- 'bc' is 'bounds check': if the dimension value (x or y) is equal to
			-- the bound value (the upper or lower bound), then math.huge is the
			-- assumed value.
			directions = {
				['up']    = {['x'] = x - 1, ['y'] = y, ['bc'] = {['dimension'] = x, ['bound'] = 1}},
				['down']  = {['x'] = x + 1, ['y'] = y, ['bc'] = {['dimension'] = x, ['bound'] = #map}},
				['left']  = {['x'] = x, ['y'] = y + 1, ['bc'] = {['dimension'] = y, ['bound'] = #column}},
				['right'] = {['x'] = x, ['y'] = y - 1, ['bc'] = {['dimension'] = y, ['bound'] = 1}}
			}

			tile_neighbors = {}
			for direction, check in pairs(directions) do
				if check.bc.dimension == check.bc.bound then
					tile_neighbors[coords_to_string(check.x, check.y)] = math.huge
				else
					tile_neighbors[coords_to_string(check.x, check.y)] = map[x][y]
				end
			end
			nodes[coords_to_string(x, y)] = {['neighbors'] = tile_neighbors, ['visited'] = false}
		end
	end
	return nodes
end

map = {
	{9,9,9,9,9,9,9,9},
	{9,1,1,1,1,1,1,9},
	{9,1,9,9,9,1,9,9},
	{9,1,9,1,1,1,1,9},
	{9,1,9,9,9,9,1,9},
	{9,9,9,9,9,9,9,9}
}

print(inspect(make_graph(map)))


